package yfmp.data.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@RequiredArgsConstructor
@NoArgsConstructor
public class AudioTrack {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NonNull private String path;

	@ManyToOne
	@NonNull private User owner;

	@ManyToMany(cascade = {CascadeType.ALL})
	@NonNull private Set<Meta> metadata;

	public void addMeta(Meta meta) {
		metadata.add(meta);
	}

	public void removeMeta(Meta meta) {
		metadata.remove(meta);
	}
}
