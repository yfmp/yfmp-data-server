package yfmp.data.domain;

import java.util.List;

import org.springframework.data.rest.core.config.Projection;

@Projection(
	name="full",
	types = { AudioTrack.class }
)
public interface FullAudioTrack {

	public String getPath();

	public User getOwner();

	public List<Meta> getMetadata();

}
