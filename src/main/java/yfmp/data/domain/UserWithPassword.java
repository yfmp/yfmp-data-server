package yfmp.data.domain;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(
	name = "userWithPassword",
	types = { Password.class }
)
public interface UserWithPassword {

	@Value("#{target.user.name}")
	public String getUsername();

	public String getPassword();

}
