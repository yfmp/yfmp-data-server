package yfmp.data.persistence;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import yfmp.data.domain.User;

public interface UserRepository extends JpaRepository<User, Long> {
	Optional<User> findByName(String name);
	Optional<User> findByEmail(String email);
}
