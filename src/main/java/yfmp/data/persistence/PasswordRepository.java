package yfmp.data.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import yfmp.data.domain.Password;
import yfmp.data.domain.User;

public interface PasswordRepository extends JpaRepository<Password, Long> {

	public Password findByUser(User user);

	@RestResource(path = "findByUsername")
	public Password findByUserName(String name);

}
