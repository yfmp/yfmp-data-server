package yfmp.data.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import yfmp.data.domain.AudioTrack;
import yfmp.data.domain.Meta;
import yfmp.data.domain.User;

public interface AudioTrackRepository extends JpaRepository<AudioTrack, Long> {
	List<AudioTrack> findByOwner(User owner);
	List<AudioTrack> findByMetadataContains(Meta meta);
}
