package yfmp.data.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import yfmp.data.domain.Meta;

public interface MetaRepository extends JpaRepository<Meta, Long> {
	List<Meta> findByKey(String key);
}
