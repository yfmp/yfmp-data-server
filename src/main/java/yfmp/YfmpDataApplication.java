package yfmp;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;

import yfmp.heartbeat.HeartbeatServer;

@SpringBootApplication
@EnableAsync
public class YfmpDataApplication extends SpringBootServletInitializer {

	@Value("${yfmp.data.heartbeatPort}")
	private int heartbeatPort;

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(YfmpDataApplication.class);
	}

	public static void main(String[] args) {
		ConfigurableApplicationContext ctx = SpringApplication.run(YfmpDataApplication.class, args);

		ctx.getBean(TaskExecutor.class)
			.execute(ctx.getBean(HeartbeatServer.class));
	}
}
