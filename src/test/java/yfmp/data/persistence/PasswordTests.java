package yfmp.data.persistence;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import yfmp.data.domain.Password;
import yfmp.data.domain.User;

@SpringBootTest
public class PasswordTests {

	@Autowired private UserRepository users;

	@Autowired private PasswordRepository passwds;

	@Test
	public void testFindByUser() throws Exception {

		User user = new User(UUID.randomUUID().toString(),
				UUID.randomUUID().toString());

		users.save(user);

		user = users.findById(user.getId())
			.orElseThrow();

		System.out.println("################################################");
		System.out.println(user);
		System.out.println("################################################");

		Password pass = new Password(user, "test");

		passwds.save(pass);

		Password res = passwds.findByUser(user);

		assertEquals(pass, res);
	}
}
